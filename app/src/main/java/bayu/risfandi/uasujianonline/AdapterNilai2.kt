package bayu.risfandi.uasujianonline

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_imgqr.view.*

class AdapterNilai2(val dt : List<HashMap<String,String>>, val fragmentNilai: FragmentNilai) :
    RecyclerView.Adapter<AdapterNilai2.HolderDataNilai>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterNilai2.HolderDataNilai {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_nilai,p0,false)

        return  HolderDataNilai(v)
    }

    override fun getItemCount(): Int {
        return dt.size
    }

    override fun onBindViewHolder(p0: AdapterNilai2.HolderDataNilai, p1: Int) {
        val data = dt.get(p1)
        p0.txtNamaSiswa.setText(data.get("nama"))
        p0.txtMplSiswa.setText(data.get("nm_mapel"))
        p0.txtNilai.setText(data.get("nilai"))
        Picasso.get().load(data.get("urlImg")).into(p0.imgBg)
        val context = p0.btnDet.getContext();
        p0.btnDet.setOnClickListener{
            val mDialogView = LayoutInflater.from(context).inflate(R.layout.layout_imgqr, null)
            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
                .setTitle("QR CODE IMAGE")
            //show dialog
            val  mAlertDialog = mBuilder.show()
            val barCodeEncoder =  BarcodeEncoder()
            val bitmap = barCodeEncoder.encodeBitmap(data.get("urlDownload"),
                BarcodeFormat.QR_CODE,400,400)
            mDialogView .imgQRCODE.setImageBitmap(bitmap)
        }
        p0.btnDown.setOnClickListener {
            fragmentNilai.openBrowser()
        }
    }

    class HolderDataNilai(v: View) : RecyclerView.ViewHolder(v){
        val txtNamaSiswa = v.findViewById<TextView>(R.id.txtNmSiswa)
        val txtMplSiswa = v.findViewById<TextView>(R.id.txtMplSiswa)
        val txtNilai = v.findViewById<TextView>(R.id.txtNilai)
        val btnDet = v.findViewById<Button>(R.id.btnShowNil)
        val imgBg = v.findViewById<ImageView>(R.id.imgBgSiswa)
        val btnDown = v.findViewById<Button>(R.id.btnDownNil)
    }
}