package bayu.risfandi.uasujianonline

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.layout_imgqr.view.*
import java.text.FieldPosition

class AdapterMapel(val dataMhs : List<HashMap<String,String>>, val fragmentMapel: FragmentMapel) :
    RecyclerView.Adapter<AdapterMapel.HolderDataMapel>(){
    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): AdapterMapel.HolderDataMapel {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_mapel,p0,false)
        return  HolderDataMapel(v)
    }

    override fun getItemCount(): Int {
        return dataMhs.size
    }

    override fun onBindViewHolder(p0: AdapterMapel.HolderDataMapel, p1: Int) {
        val data = dataMhs.get(p1)
        p0.txtMapel.setText(data.get("nm_mapel"))
        p0.txtTgl.setText(data.get("tgl_ujian"))
        p0.txtWaktu.setText(data.get("waktu_ujian"))
        p0.txtLmUjian.setText(data.get("lama_ujian"))
        p0.txtToken.setText(data.get("token"))
        p0.lyMpl.setOnClickListener({
            fragmentMapel.idMapel = data.get("id_mapel").toString()
            fragmentMapel.stPage = true
            fragmentMapel.changeScreen()
        })
        val context = p0.code.getContext();
        p0.code.setOnClickListener{
            val mDialogView = LayoutInflater.from(context).inflate(R.layout.layout_imgqr, null)
            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
                .setTitle("TOKEN SOAL")
            //show dialog
            val  mAlertDialog = mBuilder.show()
            val barCodeEncoder =  BarcodeEncoder()
            val bitmap = barCodeEncoder.encodeBitmap(data.get("token"),
                BarcodeFormat.QR_CODE,400,400)
            mDialogView .imgQRCODE.setImageBitmap(bitmap)
        }
    }

    class HolderDataMapel(v: View) : RecyclerView.ViewHolder(v){
        val txtMapel = v.findViewById<TextView>(R.id.txtNmMapel)
        val txtTgl = v.findViewById<TextView>(R.id.txtTgl)
        val txtWaktu = v.findViewById<TextView>(R.id.tglWaktu)
        val txtLmUjian = v.findViewById<TextView>(R.id.txtLmUjian)
        val lyMpl = v.findViewById<ConstraintLayout>(R.id.lyMpl)
        val jsmp = v.findViewById<TextView>(R.id.jsid)
        val txtToken = v.findViewById<TextView>(R.id.tknText)
        val code = v.findViewById<Button>(R.id.code)
    }
}

