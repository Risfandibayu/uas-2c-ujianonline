package bayu.risfandi.uasujianonline

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterMplNilai (val dt : List<HashMap<String,String>>, val fragmentNilai: FragmentNilai) :
    RecyclerView.Adapter<AdapterMplNilai.HolderDataMplNilai>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterMplNilai.HolderDataMplNilai {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_mplnilai,p0,false)
        return  HolderDataMplNilai(v)
    }

    override fun getItemCount(): Int {
        return dt.size
    }

    override fun onBindViewHolder(p0: AdapterMplNilai.HolderDataMplNilai, p1: Int) {
        val data = dt.get(p1)
        p0.txtMplNilai.setText(data.get("nm_mapel"))
        p0.txtTglMplNilai.setText(data.get("tgl_ujian"))
        p0.txtWaktuNilai.setText(data.get("waktu_ujian"))
        Picasso.get().load(data.get("urlImg")).into(p0.imgBg)
        p0.btnDet.setOnClickListener{
            fragmentNilai.idmapel = data.get("id_mapel").toString()
            fragmentNilai.changeScreen()
        }
    }

    class HolderDataMplNilai(v: View) : RecyclerView.ViewHolder(v){
        val txtMplNilai = v.findViewById<TextView>(R.id.txtMapelNilai)
        val txtTglMplNilai = v.findViewById<TextView>(R.id.txtTglMplNilai)
        val txtWaktuNilai = v.findViewById<TextView>(R.id.txtWaktuNilai)
        val btnDet = v.findViewById<Button>(R.id.btnShowNil)
        val imgBg = v.findViewById<ImageView>(R.id.imgBgNilai)
    }
}