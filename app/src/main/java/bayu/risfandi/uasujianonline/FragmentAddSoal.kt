package bayu.risfandi.uasujianonline

import android.view.View
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_add_soal.*
import kotlinx.android.synthetic.main.activity_add_soal.view.*
import kotlinx.android.synthetic.main.activity_add_soal.view.spMapel
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class FragmentAddSoal:Fragment(), View.OnClickListener  {
    lateinit var thisParent : MainActivity
    lateinit var ft : FragmentTransaction
    lateinit var addmpl : AddMapel
    lateinit var FragmentSoal : FragmentSoal
    lateinit var mediaHelper : MediaHelper
    lateinit var mplAdapter : AdapterSoalMapel
    lateinit var adapterSpin : ArrayAdapter<String>
    lateinit var mapelAdapter : ArrayAdapter<String>
    val arrayTipe = arrayOf("Pilihan Ganda","Essay")
    var daftarMapel = mutableListOf<HashMap<String,String>>()
    var listMapel = mutableListOf<String>()
    lateinit var v : View
    var url = ""
    var url2 = ""
    var url3 = ""
    var userId = ""
    var imStr = ""
    var idMapel = ""
    var stPage = false
    var tipe = 0
    var pilih_mapel = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        addmpl = AddMapel()
        FragmentSoal = FragmentSoal()
        thisParent = activity as MainActivity
        userId = thisParent.userId
        url = thisParent.mainUrl+"mapel.php"
        url2 = thisParent.mainUrl+"query_soal.php"
        url3 = thisParent.mainUrl+"soal.php"
        mplAdapter = AdapterSoalMapel(daftarMapel,FragmentSoal())
        mediaHelper = MediaHelper(thisParent)
        adapterSpin = ArrayAdapter( thisParent, android.R.layout.simple_list_item_1,arrayTipe)
        mapelAdapter = ArrayAdapter(thisParent,android.R.layout.simple_dropdown_item_1line,
            listMapel)
        v = inflater.inflate(R.layout.activity_add_soal, container, false)
        v.imgSoal.setOnClickListener(this)
        v.spMapel.onItemSelectedListener = itemSelected
        v.spTipeSoal.adapter = adapterSpin
        v.spTipeSoal.onItemSelectedListener = itemSelectedTipe
        v.spMapel.adapter = mapelAdapter
        v.fbAddUpdateSoal.setOnClickListener(this)
        v.fbDelSoal.setOnClickListener(this)
        clearInput()
//        Show hide komponen
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as AppCompatActivity).supportActionBar?.title = "Tambah Data Soal"
        super.onViewCreated(view, savedInstanceState)
    }

    fun optionHide(state : Boolean){
        if (state){
            v.txtPilihan1.visibility = View.GONE
            v.txtPilihan2.visibility = View.GONE
            v.txtPilihan3.visibility = View.GONE
            v.txtPilihan4.visibility = View.GONE
            v.txtPilihan1.setText("")
            v.txtPilihan2.setText("")
            v.txtPilihan3.setText("")
            v.txtPilihan4.setText("")
        }else{
            v.txtPilihan1.visibility = View.VISIBLE
            v.txtPilihan2.visibility = View.VISIBLE
            v.txtPilihan3.visibility = View.VISIBLE
            v.txtPilihan4.visibility = View.VISIBLE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data,v.imgSoal)
            }
        }
    }

    val itemSelectedTipe = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            tipe = 0
        }

        override fun onItemSelected( parent: AdapterView<*>?,view: View?,position: Int,id: Long) {
            if(adapterSpin.getItem(position).toString().equals("Essay")){
                tipe = 1
                optionHide(true)
            }else{
                tipe = 0
                optionHide(false)
            }
        }
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            v.spMapel.setSelection(0)
            pilih_mapel = listMapel.get(0)
        }

        override fun onItemSelected( parent: AdapterView<*>?,view: View?,position: Int,id: Long) {
            pilih_mapel = listMapel.get(position)
        }
    }

    fun lsDataMpl(namaMhs : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                listMapel.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    listMapel.add(jsonObject.getString("nm_mapel"))
                }
                mapelAdapter.notifyDataSetChanged()

            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server"+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("mode","GETALL")
                hm.put("id_user",userId)
                hm.put("nm_mapel",namaMhs)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }


    fun query_soal(mode : String){
        val request = object : StringRequest(Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(thisParent,"Operasi Berhasil "+thisParent.kdSoal,Toast.LENGTH_LONG).show()
                    clearInput()
                    thisParent.kdSoal = ""
                    thisParent.kdMapel = ""
                    backScreen()
                }else{
                    Toast.makeText(thisParent,"Operasi Gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Tidak dapat terhubung ke server"+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" -> {
                        hm.put("mode", "POST_DATA")
                        val nmFile = "DC" + SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                            .format(Date()) + ".jpg"
                        hm.put("soal", txtSoal.text.toString())
                        hm.put("pilihan_1", txtPilihan1.text.toString())
                        hm.put("pilihan_2", txtPilihan2.text.toString())
                        hm.put("pilihan_3", txtPilihan3.text.toString())
                        hm.put("pilihan_4", txtPilihan4.text.toString())
                        hm.put("answer", txtAnswer.text.toString())
                        hm.put("soal_tipe", tipe.toString())
                        hm.put("kd_mapel", pilih_mapel)
                        hm.put("image", imStr)
                        hm.put("file", nmFile)
                    }
                    "update" -> {
                        hm.put("mode", "UPDATE_DATA")
                        hm.put("id_soal",thisParent.kdSoal)
                        val nmFile = "DC" + SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                            .format(Date()) + ".jpg"
                        hm.put("soal", txtSoal.text.toString())
                        hm.put("pilihan_1", txtPilihan1.text.toString())
                        hm.put("pilihan_2", txtPilihan2.text.toString())
                        hm.put("pilihan_3", txtPilihan3.text.toString())
                        hm.put("pilihan_4", txtPilihan4.text.toString())
                        hm.put("answer", txtAnswer.text.toString())
                        hm.put("soal_tipe", tipe.toString())
                        hm.put("kd_mapel", pilih_mapel)
                        hm.put("image", imStr)
                        hm.put("file", nmFile)
                    }
                    "delete" -> {
                        hm.put("mode", "DELETE_DATA")
                        hm.put("id_soal",thisParent.kdSoal)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun getSoal(){
        val request = object : StringRequest(
            Request.Method.POST,url3,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    v.txtSoal.setText(jsonObject.getString("soal"))
                    val pos = listMapel.indexOf(jsonObject.getString("nm_mapel"))
                    v.spMapel.setSelection(pos)
                    v.spTipeSoal.setSelection(jsonObject.getString("soal_tipe").toInt())
                    v.txtPilihan1.setText(jsonObject.getString("pilihan_1"))
                    v.txtPilihan2.setText(jsonObject.getString("pilihan_2"))
                    v.txtPilihan3.setText(jsonObject.getString("pilihan_3"))
                    v.txtPilihan4.setText(jsonObject.getString("pilihan_4"))
                    v.txtAnswer.setText(jsonObject.getString("answer"))
                    Picasso.get().load(jsonObject.getString("url")).into(v.imgSoal)
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server"+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("mode","GET_BY")
                hm.put("id_soal",thisParent.kdSoal)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgSoal -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.fbAddUpdateSoal -> {
                if(thisParent.stPage){
                    query_soal("update")
                }else {
                    query_soal("insert")
                }
            }
            R.id.fbDelSoal -> {
                query_soal("delete")
            }
        }
    }

    fun backScreen(){
        v.fbAddUpdateSoal.visibility = View.GONE
        v.fbDelSoal.visibility = View.GONE
        ft = childFragmentManager.beginTransaction()
        ft.replace(R.id.frmLayout,FragmentSoal).addToBackStack(null).commit()
    }

    fun clearInput(){
        v.txtSoal.setText("")
        v.spMapel.setSelection(0)
        v.spTipeSoal.setSelection(0)
        v.imgSoal.setImageResource(R.color.colorPrimary)
        v.txtPilihan1.setText("")
        v.txtPilihan2.setText("")
        v.txtPilihan3.setText("")
        v.txtPilihan4.setText("")
        v.txtAnswer.setText("")
    }

    override fun onStart() {
        super.onStart()
        lsDataMpl("")
        getSoal()
        optionHide(true)
    }
}