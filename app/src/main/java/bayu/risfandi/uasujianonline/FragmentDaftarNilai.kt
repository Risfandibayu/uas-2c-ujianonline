package bayu.risfandi.uasujianonline

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_daftar_nilai.view.*
import org.json.JSONArray

class FragmentDaftarNilai : Fragment() {
    lateinit var thisParent : MainActivity
    lateinit var nilAdapter : AdapterNilai
    lateinit var v : View
    var daftarNilai = mutableListOf<HashMap<String,String>>()

    var url = ""
    var idUser = ""
    var idMapel = ""
    var urlDownload = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        nilAdapter = AdapterNilai(daftarNilai,this)
        url =  thisParent.mainUrl+"nilai.php"
        v = inflater.inflate(R.layout.activity_daftar_nilai, container, false)
        v.lsNilai.layoutManager = LinearLayoutManager(thisParent)
        v.lsNilai.adapter = nilAdapter
        return v
    }
    fun openBrowser(){
        var intentInternet = Intent(Intent.ACTION_VIEW, Uri.parse(urlDownload))
        startActivity(intentInternet)
    }

    override fun onStart() {
        super.onStart()
        showDataNil("")
    }

    fun showDataNil(namaSis : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarNilai.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    urlDownload = jsonObject.getString("urlDownload")
                    var nil = HashMap<String, String>()
                    nil.put("id_nilai", jsonObject.getString("id_nilai"))
                    nil.put("urlDownload", jsonObject.getString("urlDownload"))
                    nil.put("nama", jsonObject.getString("nama"))
                    nil.put("nm_mapel", jsonObject.getString("nm_mapel"))
                    nil.put("nilai", jsonObject.getString("nilai"))
                    nil.put("urlImg",jsonObject.getString("urlImg"))
                    daftarNilai.add(nil)
                }
                nilAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server"+error, Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("id_mapel",thisParent.kdMapel)
                hm.put("mode","GURU")
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

}