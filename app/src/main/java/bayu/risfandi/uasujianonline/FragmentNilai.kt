package bayu.risfandi.uasujianonline

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_daftar_nilai.view.*
import kotlinx.android.synthetic.main.activity_nilai.view.*
import org.json.JSONArray

class FragmentNilai : Fragment() {
    lateinit var thisParent : MainActivity
    lateinit var mplAdapter : AdapterMplNilai
    lateinit var nilAdapter : AdapterNilai2
    lateinit var FragmentDaftarNilai : FragmentDaftarNilai
    lateinit var v : View
    var daftarMapel = mutableListOf<HashMap<String,String>>()
    var daftarNilai = mutableListOf<HashMap<String,String>>()
    lateinit var ft : FragmentTransaction
    var url = ""
    var url2 = ""
    var urlDownload = ""
    var idmapel = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        FragmentDaftarNilai = FragmentDaftarNilai()
        thisParent = activity as MainActivity
        mplAdapter = AdapterMplNilai(daftarMapel,this)
        nilAdapter = AdapterNilai2(daftarNilai,this)
        url =  thisParent.mainUrl+"mapel.php"
        url2 = thisParent.mainUrl+"nilai.php"
        if(thisParent.tpAkun.equals("1")){
            v = inflater.inflate(R.layout.activity_nilai, container, false)
            v.lsMplNilai.layoutManager = LinearLayoutManager(thisParent)
            v.lsMplNilai.adapter = mplAdapter
        }else{
            v = inflater.inflate(R.layout.activity_daftar_nilai, container, false)
            v.lsNilai.layoutManager = LinearLayoutManager(thisParent)
            v.lsNilai.adapter = nilAdapter
        }
        return v
    }

    override fun onStart() {
        super.onStart()
        if(thisParent.tpAkun.equals("1")){
            showDataMpl("")
        }else{
            showDataNil("")
        }
    }

    fun showDataMpl(namaMpl : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMapel.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var nil = HashMap<String, String>()
                    nil.put("id_mapel", jsonObject.getString("id_mapel"))
                    nil.put("nm_mapel", jsonObject.getString("nm_mapel"))
                    nil.put("tgl_ujian", jsonObject.getString("tgl_ujian"))
                    nil.put("waktu_ujian", jsonObject.getString("waktu_ujian"))
                    nil.put("lama_ujian", jsonObject.getString("lama_ujian"))
                    nil.put("urlImg",jsonObject.getString("urlImg"))
                    daftarMapel.add(nil)
                }
                mplAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server"+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("mode","GETALL")
                hm.put("id_user",thisParent.userId)
                hm.put("nm_mapel",namaMpl)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun showDataNil(namaSis : String){
        val request = object : StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarNilai.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    urlDownload = jsonObject.getString("urlDownload")
                    var nil = HashMap<String, String>()
                    nil.put("id_nilai", jsonObject.getString("id_nilai"))
                    nil.put("urlDownload", jsonObject.getString("urlDownload"))
                    nil.put("nama", jsonObject.getString("nama"))
                    nil.put("nm_mapel", jsonObject.getString("nm_mapel"))
                    nil.put("nilai", jsonObject.getString("nilai"))
                    nil.put("urlImg",jsonObject.getString("urlImg"))
                    daftarNilai.add(nil)
                }
                nilAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server"+error, Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("id_user",thisParent.userId)
                hm.put("mode","SISWA")
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun openBrowser(){
        var intentInternet = Intent(Intent.ACTION_VIEW, Uri.parse(urlDownload))
        startActivity(intentInternet)
    }

    fun changeScreen(){
        thisParent.kdMapel = idmapel
        ft = childFragmentManager.beginTransaction()
        ft.replace(R.id.frmNilLay,FragmentDaftarNilai).commit()
        v.frmNilLay.setBackgroundColor(Color.WHITE)
        v.frmNilLay.visibility= View.VISIBLE
    }

}