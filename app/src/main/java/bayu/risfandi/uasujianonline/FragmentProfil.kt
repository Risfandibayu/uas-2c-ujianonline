package bayu.risfandi.uasujianonline

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_add_soal.view.*
import kotlinx.android.synthetic.main.activity_edit_profil.view.*
import kotlinx.android.synthetic.main.activity_profil.*
import kotlinx.android.synthetic.main.activity_profil.view.*
import kotlinx.android.synthetic.main.activity_profil.view.txtNama
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import kotlin.collections.HashMap

class FragmentProfil : Fragment(), View.OnClickListener {

    lateinit var mediaCamera: MediaCamera
    lateinit var thisParent : MainActivity
    lateinit var ft : FragmentTransaction
//    lateinit var fragmentEditProfil : FragmentEditProfil

    lateinit var v : View
    var url = ""
    var url2 = ""
    var url3 = ""
    var userId = ""
    var namaFile = ""
    var imstr = ""
    var fileUri = Uri.parse("")
    var nama = ""
    var username = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        try{
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        }catch (e: Exception){
            e.printStackTrace()
        }
        mediaCamera = MediaCamera()
        userId = thisParent.userId
        url = thisParent.mainUrl+"getprofil.php"
        url2 = thisParent.mainUrl+"updatefoto.php"
        url3 = thisParent.mainUrl+"updateprofil.php"
        v = inflater.inflate(R.layout.activity_profil, container, false)
        v.txtLogout.setOnClickListener(this)
        v.edtProfil.setOnClickListener(this)
        v.btnTkCamera.setOnClickListener(this)
        v.btnUpdPic.setOnClickListener(this)
        return v
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.txtLogout -> {
                thisParent.logout()
            }
            R.id.edtProfil -> {
                dialogEdit()
            }
            R.id.btnTkCamera -> {
                requestPermission()
            }
            R.id.btnUpdPic -> {
                updtFoto()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        showDataProfil()
        btnAwal()
    }

    fun btnAwal(){
        if(imstr.equals("")) {
            v.btnUpdPic.visibility = View.GONE
            v.btnTkCamera.visibility = View.VISIBLE
        }else{
            v.btnUpdPic.visibility = View.VISIBLE
            v.btnTkCamera.visibility = View.GONE
        }
    }

    fun dialogEdit(){
        val mDialogView = LayoutInflater.from(thisParent).inflate(R.layout.activity_edit_profil, null)
        val mBuilder = AlertDialog.Builder(thisParent).setView(mDialogView).setTitle("Edit Profil")
        val mAlertDialog = mBuilder.show()
        mDialogView.iptNama.setText(txtNama.text.toString())
        mDialogView.iptUsername.setText(txtUser.text.toString())
        mDialogView.btnUpdate.setOnClickListener {
            nama = mDialogView.iptNama.text.toString()
            username = mDialogView.iptUsername.text.toString()
            queryUpdateProfil()
            mAlertDialog.dismiss()
        }
    }


    fun showDataProfil(){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    v.txtNama.setText(jsonObject.getString("nama"))
                    v.txtUser.setText(jsonObject.getString("username"))
                    var prof = jsonObject.getString("tipe")
                    if(prof.equals("1")){
                        v.txtProfesi.setText("Guru")
                    }else{
                        v.txtProfesi.setText("Siswa")
                    }
                    if(imstr.equals("")){
                        Picasso.get().load(jsonObject.getString("url")).into(v.imgProfil)
                    }
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Perikasa koneksi internet anda", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("id_user",userId)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun requestPermission() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        fileUri = mediaCamera.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaCamera.getRcCamera())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK)
            if(requestCode == mediaCamera.getRcCamera()){
                imstr = mediaCamera.getBitmapToSting(v.imgProfil,fileUri)
                namaFile = mediaCamera.getMyFileName()
            }
    }

    fun updtFoto(){
        val request = object : StringRequest(Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response);
                val kode = jsonObject.getString("kode")
                if(kode.equals("000")){
                    Toast.makeText(thisParent,"Update foto sukses",Toast.LENGTH_LONG).show()
                    showDataProfil()
                    btnAwal()
                }else{
                    Toast.makeText(thisParent,"Update foto gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Tidak dapat terhubung ke server "+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("id_user",thisParent.userId)
                hm.put("image",imstr)
                hm.put("file",namaFile)
                return hm
            }
        }
        val q = Volley.newRequestQueue(thisParent)
        q.add(request)
    }

    fun queryUpdateProfil(){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(thisParent,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataProfil()
                }else{
                    Toast.makeText(thisParent,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Tidak dapat terhubung ke server"+error, Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("id_user",thisParent.userId)
                hm.put("nama",nama)
                hm.put("username",username)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

}