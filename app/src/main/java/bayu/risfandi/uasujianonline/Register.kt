package bayu.risfandi.uasujianonline

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONArray
import org.json.JSONObject

class Register: AppCompatActivity(), View.OnClickListener {
    val url = "http://192.168.43.200/uas-2c-webserviceujianonline/register.php"
    lateinit var adapterSpin : ArrayAdapter<String>
    val arrayTipe = arrayOf("Guru","Siswa")
    var tipe = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        supportActionBar?.title = "Register Akun"
        btnRegister.setOnClickListener(this)
        adapterSpin = ArrayAdapter( this, android.R.layout.simple_list_item_1,arrayTipe)
        sp1.adapter = adapterSpin

        sp1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if(adapterSpin.getItem(position).toString().equals("Guru")){
                    tipe = 1
                }else{
                    tipe = 0
                }
            }
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnRegister -> {
                register("register")
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }
    }

    fun register(mode : String){
        val request = object : StringRequest(Method.POST,url,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "register" -> {
                        hm.put("nama",txtNama.text.toString())
                        hm.put("username",txtUsername.text.toString())
                        hm.put("password",txtPassword.text.toString())
                        hm.put("tipe",tipe.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}