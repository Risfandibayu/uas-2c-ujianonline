package bayu.risfandi.uasujianonline

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdapterNavSoal(val dataNav : List<HashMap<String,String>>, val fragmentTestUjian: FragmentTestUjian ):
    RecyclerView.Adapter<AdapterNavSoal.HolderDataNavSoal>(){

    companion object {
        var last_position = 0
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterNavSoal.HolderDataNavSoal {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_navsoal, p0, false)
        return HolderDataNavSoal(v)
    }

    class HolderDataNavSoal(v: View) : RecyclerView.ViewHolder(v) {
        val txtNoSoal = v.findViewById<TextView>(R.id.txtNoSoal)
        val cLayout = v.findViewById<LinearLayout>(R.id.cLayout)
    }

    override fun getItemCount(): Int {
        return dataNav.size
    }

    override fun onBindViewHolder(p0: AdapterNavSoal.HolderDataNavSoal, p1: Int) {
        val dt = dataNav.get(p1)
        p0.txtNoSoal.setText(dt.get("no_soal"))
        p0.cLayout.setOnClickListener {
            last_position = p1
            notifyDataSetChanged()
            fragmentTestUjian.getSoal(dt.get("no_soal")?.toInt())
        }
        if(last_position == p1){
            p0.cLayout.setBackgroundResource(R.drawable.circle_active)
        }else{
            if(dt.get("jawaban").equals("null")){
                p0.cLayout.setBackgroundResource(R.drawable.circle_shape)
            }else{
                p0.cLayout.setBackgroundResource(R.drawable.circle_jawab)
            }
        }
    }
}