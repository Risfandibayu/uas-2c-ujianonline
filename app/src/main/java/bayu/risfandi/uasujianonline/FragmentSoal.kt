package bayu.risfandi.uasujianonline

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_soal.*
import kotlinx.android.synthetic.main.activity_soal.view.*
import kotlinx.android.synthetic.main.activity_soal.view.txtToken
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder

class FragmentSoal : Fragment(), View.OnClickListener  {
    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var ft : FragmentTransaction
    lateinit var FragmentAddSoal : FragmentAddSoal
    lateinit var mediaHelper : MediaHelper
    lateinit var mplAdapter : AdapterSoalMapel
    lateinit var mpApSoal : AdapterSoal
    lateinit var mplUjian : AdapterMplUjian
    lateinit var intentIntegrator: IntentIntegrator
    lateinit var FragmentTestUjian : FragmentTestUjian
    var daftarMapel = mutableListOf<HashMap<String,String>>()
    var daftarSoal = mutableListOf<HashMap<String,String>>()
    var daftarMplUjian =  mutableListOf<HashMap<String,String>>()


    var url = ""
    var url2 = ""
    var url3 = ""
    var url4 = ""
    var url5 = ""
    var userId = ""
    var id_soal = ""
    var id_mapel = ""
    var mapelIdtoUjian = ""
    var mplLmUjian = ""
    var stpg = false



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        FragmentAddSoal = FragmentAddSoal()
        thisParent = activity as MainActivity
        thisParent.kdSoal = ""
        FragmentTestUjian = FragmentTestUjian()
        userId = thisParent.userId
        url = thisParent.mainUrl+"mapel.php"
        url2 = thisParent.mainUrl+"soal.php"
        url3 = thisParent.mainUrl+"joinujian.php"
        url4 = thisParent.mainUrl+"getmpltoujian.php"
        url5 = thisParent.mainUrl+"mulaiujian.php"
        intentIntegrator = IntentIntegrator(thisParent)
        mplAdapter = AdapterSoalMapel(daftarMapel,this)
        mpApSoal = AdapterSoal(daftarSoal,this)
        mplUjian = AdapterMplUjian(daftarMplUjian,this)
        mediaHelper = MediaHelper(thisParent)
        v = inflater.inflate(R.layout.activity_soal, container, false)
        v.lsMapelSoal.layoutManager = LinearLayoutManager(thisParent, LinearLayoutManager.HORIZONTAL, false)
        v.lsMapelSoal.adapter = mplAdapter
        v.lsSoal.layoutManager = LinearLayoutManager(thisParent)
        v.lsSoal.adapter = mpApSoal
        v.lsMplUjian.layoutManager = LinearLayoutManager(thisParent)
        v.lsMplUjian.adapter = mplUjian
        v.btnJoin.setOnClickListener(this)
        v.showAllSl.setOnClickListener{
            id_mapel = ""
            showDataSoal("")
        }
        v.fbAddSoal.setOnClickListener{
            thisParent.kdSoal = ""
            thisParent.stPage = false
            ft = childFragmentManager.beginTransaction()
            ft.replace(R.id.frmLayout,FragmentAddSoal).commit()
            v.frmLayout.setBackgroundColor(R.color.clrBg)
            v.frmLayout.visibility = View.VISIBLE
            v.fbAddSoal.visibility = View.GONE
        }
        v.fbAddSoal.visibility = View.VISIBLE
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as AppCompatActivity).supportActionBar?.title = "Data Soal"
        super.onViewCreated(view, savedInstanceState)
    }

    fun showOrhideComponent(){
        if(thisParent.tpAkun.equals("1")){
            v.showData.visibility = View.VISIBLE
            v.joinUjian.visibility = View.GONE
            showDataSoal("")
            showDataMpl("")
        }else{
            v.showData.visibility = View.GONE
            v.joinUjian.visibility = View.VISIBLE
            v.fbAddSoal.visibility = View.GONE
            showDataMplUjian()
        }
    }

    override fun onStart() {
        super.onStart()
        thisParent.kdSoal = ""
        id_soal = ""
        v.txtToken.setText(thisParent.token)
        showOrhideComponent()
        showDataSoal("")
        showDataMpl("")
    }

    fun mulaiUjian(){
        val request = object : StringRequest(Method.POST,url5,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(thisParent,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                    gotoTest()
                }else{
                    Toast.makeText(thisParent,"Periksa tanggal dan waktu ujian kembali",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Tidak dapat terhubung ke server"+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("id_user",thisParent.userId)
                hm.put("id_mapel",mapelIdtoUjian)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }
    fun gotoTest(){
        thisParent.kdMapel = mapelIdtoUjian
        thisParent.lmUjian = mplLmUjian
        thisParent.noSoalAwal = 1
        ft = childFragmentManager.beginTransaction()
        ft.replace(R.id.frmLayout,FragmentTestUjian).commit()
        v.frmLayout.setBackgroundColor(R.color.clrBg)
        v.frmLayout.visibility= View.VISIBLE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if(intentResult != null){
            if(intentResult.contents != null){
                v.txtToken.setText(intentResult.contents)
                joinUjian()
            }else{
                Toast.makeText(thisParent,"Dibatalkan",Toast.LENGTH_SHORT).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }



    fun showDataMpl(mapel : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMapel.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mpl = HashMap<String,String>()
                    mpl.put("id_mapel",jsonObject.getString("id_mapel"))
                    mpl.put("nm_mapel",jsonObject.getString("nm_mapel"))
                    daftarMapel.add(mpl)
                }
                mplAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server"+error, Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("mode","GETALL")
                hm.put("id_user",userId)
                hm.put("nm_mapel",mapel)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun showDataMplUjian(){
        val request = object : StringRequest(
            Request.Method.POST,url4,
            Response.Listener { response ->
                daftarMplUjian.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String,String>()
                    mhs.put("id_mapel",jsonObject.getString("id_mapel"))
                    mhs.put("nm_mapel",jsonObject.getString("nm_mapel"))
                    mhs.put("tgl_ujian",jsonObject.getString("tgl_ujian"))
                    mhs.put("waktu_ujian",jsonObject.getString("waktu_ujian"))
                    mhs.put("lama_ujian",jsonObject.getString("lama_ujian"))
                    mhs.put("urlImg",jsonObject.getString("urlImg"))
                    daftarMplUjian.add(mhs)
                }
                mplUjian.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server"+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("id_user",userId)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun showDataSoal(soal: String){
        val request = object : StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarSoal.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var sl = HashMap<String,String>()
                    thisParent.kdSoal = jsonObject.getString("id_soal")
                    thisParent.kdMapel = jsonObject.getString("kd_mapel")
                    sl.put("id_soal",jsonObject.getString("id_soal"))
                    sl.put("soal",jsonObject.getString("soal"))
                    sl.put("nm_mapel",jsonObject.getString("nm_mapel"))
                    daftarSoal.add(sl)
                }
                mpApSoal.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server"+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("mode","GET_DATA")
                hm.put("id_mapel",id_mapel)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun changeScreen(){
        thisParent.kdSoal = id_soal
        thisParent.stPage = stpg
        v.fbAddSoal.visibility = View.GONE
        ft = childFragmentManager.beginTransaction()
        ft.replace(R.id.frmLayout,FragmentAddSoal).commit()
        v.frmLayout.setBackgroundColor(R.color.clrBg)
        v.frmLayout.visibility= View.VISIBLE
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnJoin -> {
                joinUjian()
            }
        }
    }

    fun joinUjian(){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(thisParent,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                    showDataMplUjian()
                }else{
                    Toast.makeText(thisParent,"Operasi Gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Tidak dapat terhubung ke server"+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("token", txtToken.text.toString())
                hm.put("id_user",thisParent.userId)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }


}