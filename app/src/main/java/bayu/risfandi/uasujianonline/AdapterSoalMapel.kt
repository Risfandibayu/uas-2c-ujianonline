package bayu.risfandi.uasujianonline

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

class AdapterSoalMapel (val dataMpl : List<HashMap<String,String>>,val fragmentSoal: FragmentSoal) :
    RecyclerView.Adapter<AdapterSoalMapel.HolderDataMapelSoal>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterSoalMapel.HolderDataMapelSoal {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_soalmapel, p0, false)
        return HolderDataMapelSoal(v)
    }

    companion object {
        var last_position = 0
    }

    override fun getItemCount(): Int {
        return dataMpl.size
    }

    override fun onBindViewHolder(p0: AdapterSoalMapel.HolderDataMapelSoal, p1: Int) {
        val data = dataMpl.get(p1)
        p0.txtMapel.setText(data.get("nm_mapel"))
        p0.itSoalMpl.setOnClickListener {
            last_position = p1
            notifyDataSetChanged()
            fragmentSoal.id_mapel = data.get("id_mapel").toString()
            fragmentSoal.showDataSoal("")
        }
        if(last_position == p1){
            p0.itSoalMpl.setBackgroundColor(Color.DKGRAY)
            p0.txtMapel.setTextColor(Color.WHITE)
        }else{
            p0.itSoalMpl.setBackgroundColor(Color.WHITE)
            p0.txtMapel.setTextColor(Color.DKGRAY)
        }
    }

    class HolderDataMapelSoal(v: View) : RecyclerView.ViewHolder(v) {
        val txtMapel = v.findViewById<TextView>(R.id.txtMapelSOal)
        val itSoalMpl = v.findViewById<ConstraintLayout>(R.id.itSoalIt)
    }
}