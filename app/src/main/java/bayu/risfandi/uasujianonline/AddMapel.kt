package bayu.risfandi.uasujianonline

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.*
import android.widget.DatePicker
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_add_mapel.view.*
//import kotlinx.android.synthetic.main.activity_add_soal.view.*
import kotlinx.android.synthetic.main.activity_mapel.*
import kotlinx.android.synthetic.main.activity_mapel.view.*
import kotlinx.android.synthetic.main.activity_mapel.view.fbAddMpl
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class AddMapel: Fragment(), View.OnClickListener {

    lateinit var thisParent : MainActivity
    lateinit var FragmentMapel : FragmentMapel
    lateinit var mplAdapter : AdapterMapel
    lateinit var ft : FragmentTransaction
    lateinit var v : View
    var cal = Calendar.getInstance()
    var url = ""
    var url2 = ""
    var userId = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        FragmentMapel = FragmentMapel()
        thisParent = activity as MainActivity
        url =  thisParent.mainUrl+"mapel.php"
        url2 = thisParent.mainUrl+"query_mapel.php"
        userId = thisParent.userId
        v = inflater.inflate(R.layout.activity_add_mapel, container, false)
        v.fbAddUpdateMpl.setOnClickListener(this)
        v.fbDelMpl.setOnClickListener(this)
        v.icDate.setOnClickListener(this)
        v.icTime.setOnClickListener(this)
        clearInput()
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as AppCompatActivity).supportActionBar?.title = "Tambah Data Mapel"
        super.onViewCreated(view, savedInstanceState)
    }

    private fun updateDateInView() {
        val myFormat = "yyyy-MM-dd" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        v.txtDateMapel!!.text = sdf.format(cal.getTime())
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.icDate -> {
                DatePickerDialog(thisParent,
                    dateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
            }
            R.id.icTime -> {
                timeDialogShow()
            }
            R.id.fbAddUpdateMpl -> {
                if(thisParent.stPage){
                    query_mapel("update")
                }else{
                    query_mapel("insert")
                }
            }
            R.id.fbDelMpl -> {
                query_mapel("delete")
            }
        }
    }

    fun query_mapel(mode : String){
        val request = object : StringRequest(Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(thisParent,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                    backScreen()
                    clearInput()
                    thisParent.kdMapel = ""
                }else{
                    Toast.makeText(thisParent,"Operasi Gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Tidak dapat terhubung ke server"+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" -> {
                        hm.put("mode","POST_DATA")
                        hm.put("nm_mapel",v.txtNamaMpl.text.toString())
                        hm.put("tgl_ujian",v.txtDateMapel.text.toString())
                        hm.put("waktu_ujian",v.txtWaktuMapel.text.toString())
                        hm.put("lama_ujian",v.txtLamaUjian.text.toString())
                        hm.put("id_user",userId)
                    }
                    "update" -> {
                        hm.put("mode","UPDATE_DATA")
                        hm.put("id_mapel",thisParent.kdMapel)
                        hm.put("nm_mapel",v.txtNamaMpl.text.toString())
                        hm.put("tgl_ujian",v.txtDateMapel.text.toString())
                        hm.put("waktu_ujian",v.txtWaktuMapel.text.toString())
                        hm.put("lama_ujian",v.txtLamaUjian.text.toString())
                        hm.put("id_user",userId)
                    }
                    "delete" -> {
                        hm.put("mode","DELETE_DATA")
                        hm.put("id_mapel",thisParent.kdMapel)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun showDataMpl(namaMhs : String){
        val request = object :StringRequest(Request.Method.POST,url,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    v.txtNamaMpl.setText(jsonObject.getString("nm_mapel"))
                    v.txtDateMapel.setText(jsonObject.getString("tgl_ujian"))
                    v.txtWaktuMapel.setText(jsonObject.getString("waktu_ujian"))
                    v.txtLamaUjian.setText(jsonObject.getString("lama_ujian"))
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server"+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("mode","GETUSER")
                hm.put("id_user",userId)
                hm.put("nm_mapel",namaMhs)
                hm.put("id_mapel",thisParent.kdMapel)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun backScreen(){
        ft = childFragmentManager.beginTransaction()
        ft.replace(R.id.frmLayoutt,FragmentMapel).addToBackStack(null).commit()
        v.fbAddUpdateMpl.visibility = View.GONE
        v.fbDelMpl.visibility = View.GONE
    }

    override fun onStart() {
        super.onStart()
        showDataMpl("")
    }

    // create an OnDateSetListener
    val dateSetListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                               dayOfMonth: Int) {
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateDateInView()
        }
    }

    fun timeDialogShow(){
        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)
            v.txtWaktuMapel.text = SimpleDateFormat("HH:mm").format(cal.time)
        }
        TimePickerDialog(thisParent, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()
    }

    fun clearInput(){
        v.txtNamaMpl.setText("")
        v.txtLamaUjian.setText("")
        v.txtDateMapel.setText("")
        v.txtWaktuMapel.setText("")
    }
}