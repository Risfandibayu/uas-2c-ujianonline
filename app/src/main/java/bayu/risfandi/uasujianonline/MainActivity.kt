package bayu.risfandi.uasujianonline

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.zxing.integration.android.IntentIntegrator
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_soal.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener  {

    var mainUrl = "http://192.168.43.200/uas-2c-webserviceujianonline/"
    lateinit var fragMap : FragmentMapel
    lateinit var fragSoal: FragmentSoal
    lateinit var fragNilai: FragmentNilai
    lateinit var fragProfil: FragmentProfil
    lateinit var ft : FragmentTransaction
    lateinit var intentIntegrator: IntentIntegrator

    val url = mainUrl+"login.php"

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val ISLOGIN = "isLogin"
    val TIPEAKUN = "tipeAKUN"
    val tp_akun = ""
    val idUser = ""
    var userId = ""
    var tpAkun = ""
    val dtLog : Int = 100
    var url3 = ""
    var urlprofil = ""
    var kdMapel = ""
    var kdSoal = ""
    var lmUjian = ""
    var noSoalAwal = 0
    var token = ""

    var stPage = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        intentIntegrator = IntentIntegrator(this)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val loginOrNot = preferences.getString(ISLOGIN, idUser).toString()
        var paket: Bundle? = intent.extras
        var stpage = paket?.getString("usrid").toString()
        var stipeAkun = paket?.getString("tipeakun").toString()
        if (loginOrNot.equals("")) {
            supportActionBar?.title = "Login Pengguna"
            setContentView(R.layout.activity_login)
            txtUsername.setText(loginOrNot)
            btnLogin.setOnClickListener(this)
            daftar.setOnClickListener(this)

        } else {
            supportActionBar?.title = "Beranda"
            setContentView(R.layout.activity_main)
            url3 = mainUrl+"joinujian.php"
            urlprofil = mainUrl+"getprofil.php"
            button.setOnClickListener(this)
            bottomNavigation.setOnNavigationItemSelectedListener(this)
            button2.setOnClickListener(this)
            button3.setOnClickListener(this)
            showDataProfil()
            var stlogin = paket?.getString("islogin").toString()
            if (stlogin.equals("yes")) {
                userId = stpage
                tpAkun = stipeAkun
                if (tpAkun.equals("1")) {
                    bottomNavigation.menu.findItem(R.id.nav_mapel).isVisible = true
                    button.visibility = View.GONE
                } else {
                    button.visibility = View.VISIBLE
                    bottomNavigation.menu.findItem(R.id.nav_mapel).isVisible = false
                }
            } else {
                userId = preferences.getString(ISLOGIN, idUser).toString()
                tpAkun = preferences.getString(TIPEAKUN, tp_akun).toString()
//                textView3.setText(""+userId)
                if (tpAkun.equals("1")) {
                    bottomNavigation.menu.findItem(R.id.nav_mapel).isVisible = true
                    button.visibility = View.GONE
                } else {
                    bottomNavigation.menu.findItem(R.id.nav_mapel).isVisible = false
                    button.visibility = View.VISIBLE
                }
            }
            fragMap = FragmentMapel()
            fragNilai = FragmentNilai()
            fragSoal = FragmentSoal()
            fragProfil = FragmentProfil()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_mapel -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragMap).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
                supportActionBar?.title = "Data Matapelajaran"
            }
            R.id.nav_soal -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragSoal).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.nav_nilai -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragNilai).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
                supportActionBar?.title = "Data Nilai"
            }
            R.id.nav_profil -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragProfil).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
                supportActionBar?.title = "Profil"
            }
            R.id.nav_home -> {
                frameLayout.visibility = View.GONE
                supportActionBar?.title = "Beranda"
            }
        }
        return true
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogin -> {
                login()
            }
            R.id.daftar -> {
                val intent =  Intent(this, Register::class.java)
                startActivity(intent)
            }
            R.id.button ->{
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.button2 -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragNilai).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
                supportActionBar?.title = "Data Nilai"
                bottomNavigation.menu.findItem(R.id.nav_soal).isChecked = true
            }
            R.id.button3 -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragNilai).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
                supportActionBar?.title = "Data Nilai"
                bottomNavigation.menu.findItem(R.id.nav_nilai).isChecked = true
            }
        }
    }

    fun login(){
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    prefEditor.putString(ISLOGIN,jsonObject.getString("id_user"))
                    prefEditor.putString(TIPEAKUN,jsonObject.getString("tipe"))
                    prefEditor.commit()
                    val intent =  Intent(this, MainActivity::class.java)
                    intent.putExtra("usrid",jsonObject.getString("id_user"))
                    intent.putExtra("tipeakun",jsonObject.getString("tipe"))
                    intent.putExtra("islogin","yes")
                    startActivityForResult(intent,dtLog)
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Periksa Username & Password Anda "+error, Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("username",txtUsername.text.toString())
                hm.put("password",txtPassword.text.toString())
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if(intentResult != null){
            if (intentResult.contents != null){
                token = intentResult.contents
                joinUjian()
            }else{
                Toast.makeText(this,"dibatalkan",Toast.LENGTH_SHORT).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)

    }

    fun logout(){
        var kosong = ""
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        prefEditor.putString(ISLOGIN,kosong)
        prefEditor.commit()
        System.exit(0)
    }

    fun joinUjian(){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                    ft = supportFragmentManager.beginTransaction()
                    ft.replace(R.id.frameLayout,fragSoal).commit()
                    frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                    frameLayout.visibility = View.VISIBLE
                    bottomNavigation.menu.findItem(R.id.nav_soal).isChecked = true
                }else{
                    Toast.makeText(this,"Operasi Gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server"+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("token", token)
                hm.put("id_user",userId)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    fun showDataProfil(){
        val request = object : StringRequest(
            Request.Method.POST,urlprofil,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    txtWelcome.setText("Selamat Datang "+jsonObject.getString("username"))
                    Picasso.get().load(jsonObject.getString("url")).into(imgWelcome)
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Perikasa koneksi internet anda", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("id_user",userId)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


}
