package bayu.risfandi.uasujianonline

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class TimeExpiredReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        TimerConfig.setTimerState(FragmentTestUjian.TimerState.Stopped, context)
        TimerConfig.setAlarmSetTime(0, context)
    }
}
