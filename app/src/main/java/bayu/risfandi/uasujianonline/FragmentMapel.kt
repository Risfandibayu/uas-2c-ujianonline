package bayu.risfandi.uasujianonline

import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_mapel.*
import kotlinx.android.synthetic.main.activity_mapel.view.*
import kotlinx.android.synthetic.main.activity_mapel.view.lsMapel
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class FragmentMapel  : Fragment(){

    lateinit var thisParent : MainActivity
    lateinit var AddMapel : AddMapel
    lateinit var mplAdapter : AdapterMapel
    lateinit var ft : FragmentTransaction
    var daftarMapel = mutableListOf<HashMap<String,String>>()
    var cal = Calendar.getInstance()
    lateinit var v : View
    var url = ""
    var url2 = ""
    var userId = ""
    var idMapel = ""
    var stPage = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AddMapel = AddMapel()
        thisParent = activity as MainActivity
        url =  thisParent.mainUrl+"mapel.php"
        userId = thisParent.userId
        mplAdapter = AdapterMapel(daftarMapel,this)
        v = inflater.inflate(R.layout.activity_mapel, container, false)
        v.lsMapel.layoutManager = LinearLayoutManager(thisParent)
        v.lsMapel.adapter = mplAdapter
        v.fbAddMpl.visibility = View.VISIBLE
        v.fbAddMpl.setOnClickListener{
            ft = childFragmentManager.beginTransaction()
            ft.replace(R.id.frmMapelLay,AddMapel).commit()
            v.frmMapelLay.setBackgroundColor(Color.WHITE)
            v.frmMapelLay.visibility = View.VISIBLE
            v.fbAddMpl.visibility = View.GONE
        }
//        v.lsMapel.addOnItemTouchListener(itemTouch)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as AppCompatActivity).supportActionBar?.title = "Data Matepelajaran"
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        showDataMpl("")
        thisParent.kdMapel = ""
    }

    fun changeScreen(){
        thisParent.kdMapel = idMapel
        thisParent.stPage = stPage
        v.fbAddMpl.visibility = View.GONE
        ft = childFragmentManager.beginTransaction()
        ft.replace(R.id.frmMapelLay,AddMapel).commit()
        v.frmMapelLay.setBackgroundColor(Color.WHITE)
        v.frmMapelLay.visibility= View.VISIBLE
    }

    fun showDataMpl(namaMhs : String){
        val request = object :StringRequest(Request.Method.POST,url,
            Response.Listener { response ->
                daftarMapel.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String, String>()
                    mhs.put("id_mapel", jsonObject.getString("id_mapel"))
                    mhs.put("nm_mapel", jsonObject.getString("nm_mapel"))
                    mhs.put("tgl_ujian", jsonObject.getString("tgl_ujian"))
                    mhs.put("waktu_ujian", jsonObject.getString("waktu_ujian"))
                    mhs.put("lama_ujian", jsonObject.getString("lama_ujian"))
                    mhs.put("token",jsonObject.getString("token"))
                    daftarMapel.add(mhs)
                }
                mplAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Terjadi kesalahan koneksi ke server"+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("mode","GETALL")
                hm.put("id_user",userId)
                hm.put("nm_mapel",namaMhs)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }


}
