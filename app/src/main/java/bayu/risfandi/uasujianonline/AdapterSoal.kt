package bayu.risfandi.uasujianonline

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

class AdapterSoal (val dataSoal : List<HashMap<String,String>>, val fragmentSoal: FragmentSoal) :
    RecyclerView.Adapter<AdapterSoal.HolderDataSoal>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterSoal.HolderDataSoal {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_soal, p0, false)
        return HolderDataSoal(v)
    }

    override fun getItemCount(): Int {
        return dataSoal.size
    }

    override fun onBindViewHolder(p0: HolderDataSoal, p1: Int) {
        val data = dataSoal.get(p1)
        p0.txtNmSoal.setText(data.get("soal"))
        p0.txtNmMapel.setText(data.get("nm_mapel"))
        p0.cLayout.setOnClickListener({
            fragmentSoal.id_soal = data.get("id_soal").toString()
            fragmentSoal.stpg = true
            fragmentSoal.changeScreen()
        })
    }

    class HolderDataSoal(v: View) : RecyclerView.ViewHolder(v) {
        val txtNmSoal = v.findViewById<TextView>(R.id.txtNmSoal)
        val txtNmMapel = v.findViewById<TextView>(R.id.txtNamaMapel)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }

}