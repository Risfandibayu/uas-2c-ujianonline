package bayu.risfandi.uasujianonline

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterMplUjian (val dataUjian : List<HashMap<String,String>>, val fragmentSoal: FragmentSoal ):
    RecyclerView.Adapter<AdapterMplUjian.HolderDataUjian>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterMplUjian.HolderDataUjian {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_mplujian, p0, false)
        return HolderDataUjian(v)
    }

    override fun getItemCount(): Int {
        return dataUjian.size
    }

    override fun onBindViewHolder(p0: AdapterMplUjian.HolderDataUjian, p1: Int) {
        val dt = dataUjian.get(p1)
        p0.txtNmUjian.setText(dt.get("nm_mapel"))
        p0.txtTglUjian.setText(dt.get("tgl_ujian"))
        p0.txtWaktuUjian.setText(dt.get("waktu_ujian"))
        Picasso.get().load(dt.get("urlImg")).into(p0.imgBg)
        p0.btnPlay.setOnClickListener {
            fragmentSoal.mapelIdtoUjian = dt.get("id_mapel").toString()
            fragmentSoal.mplLmUjian = dt.get("lama_ujian").toString()
            fragmentSoal.mulaiUjian()
        }
    }

    class HolderDataUjian(v: View) : RecyclerView.ViewHolder(v) {
        val txtNmUjian = v.findViewById<TextView>(R.id.txtMapelUjian)
        val txtTglUjian = v.findViewById<TextView>(R.id.txtTglMplUjian)
        val txtWaktuUjian = v.findViewById<TextView>(R.id.txtWaktuUjian)
        val imgBg = v.findViewById<ImageView>(R.id.imgBg)
        val btnPlay = v.findViewById<Button>(R.id.btnShowNil)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.ujLayout)
    }
}