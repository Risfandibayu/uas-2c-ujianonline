package bayu.risfandi.uasujianonline

import android.app.AlarmManager
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_test.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class FragmentTestUjian : Fragment(), View.OnClickListener {
    lateinit var thisParent : MainActivity
    lateinit var navAdapter : AdapterNavSoal
    lateinit var ft : FragmentTransaction
    lateinit var FragmentSoal : FragmentSoal
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View

    private lateinit var timer: CountDownTimer
    private var timerLengthSeconds = 0L
    private var timerState = TimerState.Running
    private var secondRemaining = 0L

    var daftarNav = mutableListOf<HashMap<String,String>>()
    var url = ""
    var url2 = ""
    var url3 = ""
    var url4 = ""
    var userId = ""
    var kdMapel = ""
    var lmujian = 0
    var tmUjian = 0L
    var currentPage = 0
    var idSoal = 0
    var idSoalTemp = 0
    var jawaban = ""
    var cekJawab = ""

    companion object {
        fun setAlarm(context: Context, nowSeconds: Long, secondsRemaining: Long): Long {
            val wakeUpTime = (nowSeconds + secondsRemaining) * 1000
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val intent = Intent(context,TimeExpiredReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(context,0,intent,0)
            alarmManager.setExact(AlarmManager.RTC_WAKEUP,wakeUpTime,pendingIntent)
            TimerConfig.setAlarmSetTime(nowSeconds,context)
            return wakeUpTime
        }

        fun removeAlarm(context: Context){
            val intent = Intent(context,TimeExpiredReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(context,0,intent,0)
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(pendingIntent)
            TimerConfig.setAlarmSetTime(0,context)
        }

        val nowSeconds: Long
            get() = Calendar.getInstance().timeInMillis / 1000
    }

    enum class TimerState{
        Stopped, Paused, Running
    }

    override fun onStart() {
        super.onStart()
        getJawaban()
        getSoal(1)
        showNavSoal()
        v.txtTimer.visibility = View.GONE
        v.tombolArea.visibility = View.VISIBLE
        v.soalll.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        initTimer()
        removeAlarm(thisParent)
    }

    override fun onPause() {
        super.onPause()
        if(timerState == TimerState.Running){
            timer.cancel()
            val wakeUpTime = setAlarm(thisParent, nowSeconds,secondRemaining)
        }
        else if(timerState == TimerState.Paused){

        }

        TimerConfig.setPreviousTimerLengthSeconds(timerLengthSeconds,thisParent)
        TimerConfig.setSecondsRemaining(secondRemaining,thisParent)
        TimerConfig.setTimerState(timerState,thisParent)
    }

    private fun initTimer(){
        timerState = TimerConfig.getTimerState(thisParent)
        if(timerState == TimerState.Stopped)
            setNewTimerLength()
        else
            setPreviousTimerLength()

        secondRemaining = if(timerState == TimerState.Running || timerState == TimerState.Paused)
            TimerConfig.getSecondsRemaining(thisParent)
        else
            timerLengthSeconds

        val alarmSetTime = TimerConfig.getAlarmSetTime(thisParent)
        if(alarmSetTime > 0)
            secondRemaining -= nowSeconds - alarmSetTime
        if(secondRemaining <= 0)
            onTimerFinished()
        if(timerState == TimerState.Running)
            startTimer()
        updateCountdownUI()
    }

    private fun onTimerFinished(){
        timerState = TimerState.Stopped
        setNewTimerLength()
        TimerConfig.setSecondsRemaining(timerLengthSeconds,thisParent)
        secondRemaining = timerLengthSeconds
        updateCountdownUI()
        v.btnJawab.visibility = View.GONE
        thisParent.bottomNavigation.visibility = View.VISIBLE
    }

    private fun startTimer(){
        timerState = TimerState.Running
        timer = object : CountDownTimer(secondRemaining * 1000, 1000){
            override fun onFinish() = onTimerFinished()

            override fun onTick(millisUntilFinished: Long) {
                secondRemaining = millisUntilFinished / 1000
                updateCountdownUI()
                v.txtTimer.visibility = View.VISIBLE
                v.btnJawab.visibility = View.VISIBLE
                v.btnStart.visibility = View.GONE
                v.soalll.visibility = View.VISIBLE
                thisParent.bottomNavigation.visibility = View.GONE
            }
        }.start()
    }

    private fun setNewTimerLength(){
        val lengthMinutes = TimerConfig.getTimerLength(thisParent)
        timerLengthSeconds = (lengthMinutes * tmUjian)
    }

    private fun setPreviousTimerLength(){
        timerLengthSeconds = TimerConfig.getPreviousTimerLengthSeconds(thisParent)
    }

    private fun updateCountdownUI(){
        val minutesUntilFinished = secondRemaining / 60
        val secondsInMinuteUntilFinished = secondRemaining - minutesUntilFinished * 60
        val secondStr = secondsInMinuteUntilFinished.toString()
        v.txtTimer.text = "$minutesUntilFinished:${
        if(secondStr.length == 2) secondStr
        else "0" + secondStr }"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        FragmentSoal = FragmentSoal()
        thisParent = activity as MainActivity
        dialog = AlertDialog.Builder(thisParent)
        userId = thisParent.userId
        kdMapel = thisParent.kdMapel
        url = thisParent.mainUrl+"testujian.php"
        url2 = thisParent.mainUrl+"showallsoal.php"
        url3 = thisParent.mainUrl+"jawabsoal.php"
        url4 = thisParent.mainUrl+"finishujian.php"
        navAdapter = AdapterNavSoal(daftarNav,this)
        v = inflater.inflate(R.layout.activity_test, container, false)
        v.lsNavSoal.layoutManager = LinearLayoutManager(thisParent, LinearLayoutManager.HORIZONTAL, false)
        v.lsNavSoal.adapter = navAdapter
        v.btnStart.setOnClickListener{
            startTimer()
            timerState == TimerState.Running
        }
        v.btnJawab.setOnClickListener(this)
        v.btnFinish.setOnClickListener(this)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as AppCompatActivity).supportActionBar?.title = "Tes Ujian"
        super.onViewCreated(view, savedInstanceState)
    }

    fun backScreen(){
        ft = childFragmentManager.beginTransaction()
        ft.replace(R.id.frmBack,FragmentSoal).addToBackStack(null).commit()
    }

    fun getJawaban(){
        if(v.option1.isChecked == true){
            cekJawab=""
            v.option2.isChecked = false
            v.option3.isChecked = false
            v.option4.isChecked = false
            jawaban = v.option1.text.toString()
        }
        if(v.option2.isChecked == true){
            v.option1.isChecked = false
            v.option3.isChecked = false
            v.option4.isChecked = false
            jawaban = v.option2.text.toString()
        }
        if(v.option3.isChecked == true){
            v.option2.isChecked = false
            v.option1.isChecked = false
            v.option4.isChecked = false
            jawaban = v.option3.text.toString()
        }
        if(v.option4.isChecked == true){
            v.option2.isChecked = false
            v.option3.isChecked = false
            v.option1.isChecked = false
            jawaban = v.option4.text.toString()
        }
        if(v.txtJawaban.text.toString().isNotEmpty()){
            jawaban = v.txtJawaban.text.toString()
        }
    }

    fun getSoal(noSoal: Int?){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    lmujian = jsonObject.getInt("lama_ujian")
                    tmUjian = lmujian.toLong()
                    currentPage = jsonObject.getInt("no_soal")
                    idSoalTemp = jsonObject.getInt("id_soal_temp")
                    idSoal = jsonObject.getInt("id_soal")
                    v.txtNoSoalTest.setText(jsonObject.getString("no_soal")+".")
                    v.txtSoalTest.setText(jsonObject.getString("soal"))
                    v.option1.setText(jsonObject.getString("pilihan_1"))
                    v.option2.setText(jsonObject.getString("pilihan_2"))
                    v.option3.setText(jsonObject.getString("pilihan_3"))
                    v.option4.setText(jsonObject.getString("pilihan_4"))
//
                    if(jsonObject.getString("jawaban_anda").equals("null")){
                        v.txtJawaban.setText("")
                    }else{
                        v.txtJawaban.setText(jsonObject.getString("jawaban_anda"))
                    }

                    if(v.option1.text.toString().equals(jsonObject.getString("pilihan_1"))){
                        v.option1.isChecked = true
                    }
                    if(v.option2.text.toString().equals(jsonObject.getString("pilihan_2"))){
                        v.option2.isChecked = true
                    }
                    if(v.option3.text.toString().equals(jsonObject.getString("pilihan_3"))){
                        v.option3.isChecked = true
                    }
                    if(v.option4.text.toString().equals(jsonObject.getString("pilihan_4"))){
                        v.option4.isChecked = true
                    }

                    Picasso.get().load(jsonObject.getString("urlImg")).into(v.imgSoalJwb)
                    if(jsonObject.getString("soal_tipe").equals("1")){
                        v.options.visibility = View.GONE
                        v.txtJawaban.visibility = View.VISIBLE
                    }else{
                        v.options.visibility = View.VISIBLE
                        v.txtJawaban.visibility = View.GONE
                    }
                    if(jsonObject.getString("gambar_soal").equals(null)){
                        v.imgSoalJwb.visibility = View.GONE
                    }else{
                        v.imgSoalJwb.visibility = View.VISIBLE
                    }
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Perikasa koneksi internet anda "+error, Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("id_user", userId)
                hm.put("id_mapel",kdMapel)
                hm.put("no_soal",noSoal.toString())
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun showNavSoal(){
        val request = object : StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarNav.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var nav = HashMap<String,String>()
                    nav.put("no_soal",jsonObject.getString("no_soal"))
                    nav.put("jawaban",jsonObject.getString("jawaban_anda"))
                    daftarNav.add(nav)
                }
                navAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Perikasa koneksi internet anda", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("id_user", userId)
                hm.put("id_mapel",kdMapel)
                hm.put("mode","GET_ALL")
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun kerjakanSoal(){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(thisParent,"Soal berhasil dijawab",Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(thisParent,"Gagal Menyimpan",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Tidak dapat terhubung ke server "+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("id_soal_temp", idSoalTemp.toString())
                hm.put("id_user",thisParent.userId)
                hm.put("id_soal",idSoal.toString())
                hm.put("id_mapel",kdMapel)
                hm.put("answer",jawaban)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
    }

    fun finish(){
        val request = object : StringRequest(Method.POST,url4,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(thisParent,"Ujian telah selesai dikerjakan",Toast.LENGTH_LONG).show()
                    backScreen()
                }else{
                    Toast.makeText(thisParent,"Gagal Menyimpan",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(thisParent,"Tidak dapat terhubung ke server "+error,Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("id_soal_temp", idSoalTemp.toString())
                hm.put("id_user",thisParent.userId)
                hm.put("id_soal",idSoal.toString())
                hm.put("id_mapel",kdMapel)
                hm.put("answer",jawaban)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(thisParent)
        queue.add(request)
        timerState = TimerState.Stopped
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        finish()
        thisParent.bottomNavigation.visibility = View.VISIBLE
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnJawab -> {
                getJawaban()
                kerjakanSoal()
            }
            R.id.btnFinish -> {
                dialog.setTitle("Konfirmasi").setIcon(R.drawable.ic_info)
                    .setMessage("Apakah anda yakin menyelesaikan ujian ini?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
        }
    }

}